---
title: "Pouring my heart and mind out: Sharing where I have been to in my mind and what I have seen there"
date: 2024-04-20
# weight: 1
# aliases: ["/first"]
tags: ["first"]
categories: ["meta"]
author: "Alp"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
---

I really like reading some writing on others' websites, I feel like I know the person a bit better than I normally would, I get to spend some time checking their ideas out, they get to show me a bit of their minds. Sometimes, texts of random people on the internet inspire me to think deeply, sometimes they even manage to shift the way I see things. Like those internet strangers, or anybody else, I think and feel too. I have my opinions and a lot of questions. I hate some things and love others.

Writing openly and letting people into your mind is not the easiest thing. After sharing your thoughts online, you don't get to choose who sees them, or if those people like what you have to say. It gives everyone the possibility to develop a mild dislike for you, without having to meet you in person, they don't even need to see your face or hear your voice. We are all people and want to be liked by others, this certainly makes it harder to share your thoughts with your name clearly attached. - This is fine. I believe most people come to the conclusion that views they don't agree with are not a reason on their own to hate the person describing what they see. This is, assuming they actively think about this, otherwise we probably dislike each other for holding different opinions...

Thoughts are not context-independent. What I think makes sense in a specific context, and ideas can easily become nonsensical when taken out of the context they originated in. I can try to provide as much information as possible about the way I see the world and why, but you still don't get a bundle with my mind and feelings included. - And I think this is beautiful and intriguing. What *I* write can trigger some things in *you*, and what you get to enjoy on your side is probably similar but certainly not identical to the *thing* that inspired my thoughts and words. One thought can be unterstood in as many ways as there are people.

My opinions change all the time. As I simply exist longer, spend more time with my questions and maybe add new ones to the list, I tend to see things differently. I think there is no need to force myself to settle on opinions and perspectives, it is only natural to think differently as I experience more of my life. What I think about also changes often: questions and problems come into and go out of fashion. Sometimes I realize how a question I thought about daily or a problem I was fixated on a year ago seems less important (or maybe even outright irrelevant!), my interests change. - So I don't intend to stick to the things I wrote in the past, I don't claim that I am developing a coherent set of beliefs. I can add updates to the end of individual pages when I have a slight change of mind, and for changes that are significant enough, I can write a separate entry and add a link to the new page.

---

So take this as something similar to a (travel) vlog. People share what they do in their private lives on social media all the time. When they travel to new places, they film their moments, they take photos of the beautiful, the unusual and the famous. They proudly share them with their audience.

When I write about some random thought or feeling I had while going home at the end of the day, it is me posting a story with the remarkable meal I had for lunch on an otherwise unremarkable day. When I write about a cool idea I came across while reading a book or watching a TED talk, it is me happily posing in front of the Eiffel Tower. And when I write about an idea I've put some effort into, it is me posting a photo of a painting I have been working on for some time.

I want to pour my heart *and mind!* out. I want to share where I have been to in my mind, and what interesting things I've seen there.

---

TLDR;
- Sharing what you think and how you feel with internet strangers is hard but internet strangers are mostly friendly.
- I can not convey meaning exactly as I have it in my mind, and this is a beautiful thing.
- What I *think* and what I *think about* change all the time.
- I want to share where I have been to in my mind and what I have seen there. I want to pour my heart and mind out.
